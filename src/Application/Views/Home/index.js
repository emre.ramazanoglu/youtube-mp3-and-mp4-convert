import React, { Component } from 'react'
import Service from '../../controls/Service'
import Navbar from '../Components/Navbar'
import Search from '../Components/Search'
import {  Link } from "@reach/router"

export default class componentName extends Component {
  state = {
    data:[],
    isLoading:true
  }
 async componentDidMount(){
  var data = await Service.MostVideo()
  console.log(data)
  this.setState({data:data.items,isLoading:false})
  }
  render() {
    if(this.state.isLoading){
      return "Yükleniyor"
    }
    return (
      <>
      <Navbar navigate={this.props.navigate} />
      <div class="container">
      <Search navigate={this.props.navigate}/>
      <br/>
        <div class="card-columns">
         
         {this.state.data.map(ver=> <div class="card">
            <img class="card-img-top" src={ver.snippet.thumbnails.default.url} height="250" alt=""/>
            <div class="card-body">
              <h4 class="card-title"><Link to={`/video/${ver.id}`}>{ver.snippet.title}</Link></h4>
              <div className="text-right">
                <button className="btn btn-success" onClick={()=>this.props.navigate(`/video/${ver.id}`)}>İndir</button>
                </div>
            </div>
          </div>)}
          
        </div>
      </div>
      </>
    )
  }
}
