import React, { Component } from 'react'

export default class componentName extends Component {
    state={
        key:""
    }

    Search () {
        if(this.state.key == ""){
            alert('boş alan')
        }
        else{
            this.props.navigate(`/search/${encodeURIComponent(this.state.key)}`)
            this.setState({key:""})
        }
    }

  render() {
    return (
      <>
        <div className="row">
            <div className="col-sm-11">
		  <input autofocus="autofocus" autocomplete="off" id="query" type="text" name="ara" class="form-control " onChange={(text)=>this.setState({key:text.target.value})} placeholder="Sanatçı ya da Şarkı Adı Yazınız...." required=""/>
            </div>   
            <div className="col-sm-1">
                <button className="btn btn-success" onClick={()=>this.Search()}>Search</button>
            </div>   
        </div>        
      </>
    )
  }
}
