import React, { Component } from 'react'
import Service from '../../controls/Service'
import Navbar from '../Components/Navbar'

export default class componentName extends Component {
    state = {
        data:[],
        isLoading:true,
        video:''
      }
     async componentDidMount(){

      var data = await Service.VideoDetail(this.props.id)
      console.log(data)
      this.setState({data:data.items,isLoading:false})

      }

      Mp4Convert = async () => {
       if(this.state.video == ""){
        var video  = await Service.Mp4Convert(this.props.id)
        this.setState({video:video.videoUrl})
        window.open(`${video.videoUrl}&title=ascsac`,'_blank')
       }else{
        window.open(`${this.state.video}&title=ascsac`,'_blank')

       }
        // var video  = await Service.Mp4Convert(this.props.id)
        // this.setState({video:video.videoUrl})

      }
  render() {
    if(this.state.isLoading){
        return ""
      }
      var data = this.state.data[0]
    return (
      <>
      <Navbar/>
       <div class="container">
       <div class="card">
       <iframe width="100%" height="500" src={`https://www.youtube.com/embed/${this.props.id}`}frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        <hr/>
            <div class="row">
                    <button type="button" class="list-group-item list-group-item-action bg-dark  col-sm-6 text-center"  onClick={()=>this.Mp4Convert()}  style={{color:'white',paddingLeft:'8px'}}>Mp4 İndir</button>
                <iframe width="500px" height="60px" scrolling="no" style={{border:'none'}} src={`https://www.download-mp3-youtube.com/api/?api_key=Mjk1MTg5MTUw&format=mp3&logo=1&video_id=${this.props.id}`}></iframe>

            </div>
        <hr/>
      <div class="card-body">
                <h4 class="card-title">{data.snippet.title}</h4>
                <p class="card-text">{data.snippet.description}</p>
            </div>
        </div>    
       </div>
      <div>

  {/* Modal */}
  <div className="modal fade" id="modelId" tabIndex={-1} role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div className="modal-dialog" role="document">
      <div className="modal-content">
        <div className="modal-header">
          <h5 className="modal-title">Modal title</h5>
          <button type="button" className="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div className="modal-body">
          <div class="list-group">
              <button type="button" class="list-group-item list-group-item-action active">Active item</button>
          </div>
        </div>
        <div className="modal-footer">
          <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="button" className="btn btn-primary">Save</button>
        </div>
      </div>
    </div>
  </div>
</div>

      </>
    )
  }
}
