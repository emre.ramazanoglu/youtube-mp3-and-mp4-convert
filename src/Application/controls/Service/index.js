import React, { Component } from 'react'
import Api from '../Api/'

export default{

    MostVideo: async  (sef)=>{
        var data = await Api.get(`videos?part=snippet&chart=mostPopular&regionCode=tr&maxResults=50`);
        return data;
    },
    VideoDetail:async (id)=>{
        return await Api.get(`videos?id=${id}&part=snippet`)
    },
    Mp4Convert:async (id) => {
        return await Api.postham(`http://emreramazanoglu.com/api/youtube/?id=${id}`)
    },
    Search:async (name) => {
        return await Api.get(`search?part=snippet&q=${name}&maxResults=50`)
    }
}