import React, { Component } from 'react'
import { Router, Link } from "@reach/router"
import Home from './Application/Views/Home'
import Detail from './Application/Views/Detail'
import Search from './Application/Views/Search'

export default class componentName extends Component {
  render() {
    return (
     <Router>
      <Home path="/" />
      <Search path="/search/:name" />
      <Detail path="/video/:id" />
    </Router>
    )
  }
}
